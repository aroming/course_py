---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "Ops, Exps and Objs"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 内置对象、运算符、表达式、关键字Built-In Objects, Operators, Expressions and Keywords

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 对象Objects

---

### 一切皆对象Everything is Object

1. 对象是`Python`中的基本概念，在`Python`中 **一切皆对象**
    1. 一切皆对象 == machine ==> all is `pointer` and in `heap memory`
    1. 一切皆对象 == programmer ==> `data` + `behaviors`
    1. 一切皆对象 == get info ==> `type()`, `id()`/`address`, `value`
    1. `type` = instance => `class` = instance => `object`
    1. `variable`, `function`, `class`都是`object`

>[Differences Between Python Types and Objects](https://colinoftroy.wordpress.com/2012/12/03/differences-between-python-types-and-objects/)

---

### 内置、标准库、扩展库Built-In, Standard Library and Extension Package

1. `built-in`可直接使用，非`built-in`需`import`（与`Shell built-in command`类似）
1. `built-in` $\subset$ `standard library`
1. `extension package`需要额外安装

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 内置Built-In

---

```python
dir(__builtins__) #列出所有内置对象
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 关键字、标识符与变量Key Words, Identifier and Variable

---

```python {.line-numbers}
import keyword

keyword.kwlist #输出当前版本的所有关键字
```

---

### Python标识符命名规则Rules of Python Identifier Naming

1. 强烈规则
    1. 由字符、下划线和数字组成，不能以数字开头，不能有空格和标点符号
    1. 不能使用关键字
    1. 大小写敏感
1. 建议规则
    1. 不建议与内置或已导入名称相同
    1. 语法上允许使用非`ASCII`字符，但不建议

---

### 常量与变量Constant and Variable

1. `variable变量`: 值可变的内存单元
1. `constant常量`: 一种特殊的`variable`，值不能改变的变量
1. `literal字面量`: 直接写在源代码的值

:exclamation: 在`Python`中，`variable`实际上是`Python解释器`里的`name`，`name`本身没有类型，`name`指向的`object`有类型

:point_right: 为与主流描述一致，以下仍使用`variable`指代`name`

---

### Python变量的特点Traits of Python Variable

1. 变量不需要声明，变量必须被定义（赋值即定义）后才能使用
1. 变量指向对象（即，存储对象的内存地址），运行过程中，变量可指向不同类型的对象（即： _**动态（类型）语言**_）
1. 允许多个变量在一个表达式中赋值

---

```python {.line-numbers}
a = b = c = 1

a, b, c = 'a', 'b', "long long string"
```

---

:question: `声明declare` vs `定义define`

:question: `指针pointer` in `C/C++`

:question: `左值lvalue` vs `右值rvalue`

:question: `强类型strong typing` vs `弱类型weak typing` _**and**_ `静态类型static typing` vs `动态类型dynamic typing`

><https://cloud.tencent.com/developer/article/1332131>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 类型Types

---

### 基本内置类型Basic Built-In Types

1. `Numeric数值`: `int`, `float`, `complex`
1. `Collection集合`/`Container容器`
    1. `String字符串`/`Character Array字符数组`: `str`(immutable)
    1. `Byte Array字节数组`: `bytes`(immutable)/`bytearray`(mutable)
    1. `Object Array对象数组`: `list`(immutable)/`tuple`(mutable)
    1. `Object Set对象集合`: `set`
    1. `Object Map对象映射`: `dict`

---

### 基本内置类型（续）Basic Built-In Types (contd.)

1. `Boolean布尔`: `bool`
1. `NULL空`: `NoneType`

---

### 基本内置类型（再续）Basic Built-In Types (contd. again)

1. `Exception`
1. `File`
1. `...`

---

### 数值/数字Numeric/Number

1. 数值大小无限制
1. `int`: 动态内存分配、占用空间不定
    1. `0b`开头的字面量代表二进制
    1. `0o`开头的字面量代表八进制
    1. `0x`开头的字面量代表十六进制
1. `float`： 占用空间固定、存在精度误差
1. `complex`

:warning: 算术运算时会进行`implicit type casting`隐式类型转换

---

```python {.line-numbers}
import sys

print(sys.getsizeof(100))
print(sys.getsizeof(int()))
print(sys.getsizeof(int))
print(sys.getsizeof(12345678901234567890))
print(sys.getsizeof(123456789012345678901234567890))

print(sys.getsizeof(100.0))
print(sys.getsizeof(float()))
print(sys.getsizeof(float))
print(sys.getsizeof(12345678901234567890.0))
print(sys.getsizeof(12345678901234567890.12345678901234567890))
```

---

```python
print(99999999 ** 9)

print(0.4 -0.1)

print(0.4 - 0.1 == 0.3)

print(abs(0.4 -0.1 < 0.3) < 1e-6>)

x = 3 + 4j
y = 5 + 6j
print(x + y)

print(123_456_789)
```

---

### String and Bytes

1. `String`： `Unicode`编码存储、`Unicode`解码显示，单引号和双引号的效果一致（为什么要这样设计:question:）
    1. 单行字符串： 单引号`''`， 双引号`""`
    1. 多行字符串： 三单引号`'''`， 三双引号`"""`
1. `Bytes`： 字节直接存储、`ASCII`解码显示，文本字面量前加`b`

:warning: `Python`没有`char`(`Character`)

:question: `Character` vs `String`

---

### String

1. 转义字符： `\n`, `\t`, `\"`, `\'`等有特殊含义的字符
1. 原始字符串（`raw`）： 字符串前加`r`表示不进行转义

---

```python {.line-numbers}
demo_str_01 = 'Hello World.'
demo_str_02 = "Python is a great language."
demo_str_03 = '''This is a
"long long"
'multi-line'
string'''
demo_str_04 = r"this is a raw \n string"
```

---

### Boolean

1. `bool`继承自`int`
1. 只有`True`和`False`两个值
1. `assert (True == 1 and False == 0)`

---

### Collections

---

|--|`list`|`tuple`|`dict`|`set`|
|--|:--:|:--:|:--:|:--:|
|定界符|`[]`|`()`|`{}`|`{}`|
|分隔符|`,`|`,`|`,`|`,`|
|值可变|Y|N|Y|Y|
|自动排序|N|N|N|Y|
|下标访问|Y|Y|Y(`K`)|N|
|值的要求|N|N|`K` => hashable|hashable|
|值可重复|Y|Y|`K` => N, `V` => Y|N|
|查找速度|最慢|慢|快|快|
|插入速度|尾部快|N/A|快|快|

---

```python {.line-numbers}
demo_list = [1, 2, 3]
demo_tuple = (1, 2, 3)
demo_dict = {'a':97, 'b':98, 'c':99}
demo_set = {1, 2, 3}
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 运算符和表达式Operators and Expressions

---

### 运算符优先级Operator Precedence

1. 需记忆的运算符优先级： `.属性运算符` > `算术运算符` > `比较（关系）运算符` > `单目逻辑运算符` > `赋值运算符` > `双目逻辑运算符`
1. 复杂的运算表达式建议使用`()`

:warning: `赋值运算符` > `双目逻辑运算符`

---

### Special Usages in Python - 算术运算符Arithmetic Operators

1. `+` => 除用于算术加法外，还可用于`list`/`tuple`/`str`连接
1. `*` => 除用于算术乘法外，还可用于`list`/`tuple`/`str`重复
1. `//` => `int`或`float`向下求整商（`floor division地板除`）
1. `%` => 除用于`int`求余数外，还可用于`float`求余数、`str`格式化
1. `**` => 幂乘

---

```python {.line-numbers}
print("%c, %d"%(65, 65))
```

---

### Special Usages in Python - Relational Operators

1. 可以连用关系运算符（:warning: 不建议使用）
    + `1 < n < 5` = => `1 < n and n < 5`
1. 可以用于数据结构
    + `[1, 2, 3] < [1, 2, 4]`
    + `{1, 2, 3} < {1, 2, 3, 4}`

---

### Special Usages in Python - Logical Operators

1. `and`和`or`表达式的值是最后一个被计算的表达式的值，而不一定是`bool` ==> `assert (3 and 5) == 5`
1. `not`表达式的值一定是`True`或`False`
1. `is not`
1. `not in`

---

### `is` vs `==`

1. `is`： 比较两个对象的内存地址是否相同，`a is b`等价于`id(a) is id(b)`
1. `==`： 调用对象的`__eq()__`方法，`a == b`等价于`a.__eq__(b)`

---

### Special Usages in Python - Member Operators

1. `in` => logical test
1. `for ... in ...` => traverse

---

### Special Usages in Python - Set Operators

1. `&` => 交集
1. `|` => 并集
1. `-` => 差集
1. `^` => 对称差集（并集 - 差集）

---

### Special Usages in Python - Increment and Decrement Operators

:warning: `Python`没有自增/自减运算符

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 常用内置函数Common Used Built-In Function

---

### 数据类型转换

|Function函数|Description描述|
|-|-|
|int(x [,base]) |将x转换为一个整数|
|bin(x)|将整数转换成二进制|
|oct(x)|将整数转换成八进制|
|hex(x)|将整数转换成十六进制|
|float(x)|将x转换到一个浮点数|
|complex(real [,imag])|创建一个复数|

---

### 数据类型转换（续）

|Function函数|Description描述|
|-|-|
|ord(x)|返回字符的`Unicode`编码|
|chr(x)|返回`Unicode`编码对应的字符|
|str(x)|将x转换为字符串|
|repr(x)|将x转换为表达式字符串|

---

### 数据类型转换（再续）

|Function函数|Description描述|
|-|-|
|eval(str)|将字符串运行成表达式，并返回运算结果|
|tuple(s)|将序列转换为一个元组|
|list(s)|将序列转换为一个列表|
|set(s)|转换为可变集合|
|dict(s)|转换成`dict`|

---

### 类型

|Function函数|Description描述|
|-|-|
|`type(x)`|
|`isinstance(x)`|

---

### 最值与求和

|Function函数|Description描述|
|-|-|
|`max()`|
|`min()`|
|`sum()`|

---

### 输入输出

|Function函数|Description描述|
|-|-|
|`input()`|返回用户输入的字符串|
|`print()`|

---

### 排序

|Function函数|Description描述|
|-|-|
|`sorted()`|
|`reversed(x)`|翻转

---

### 枚举与迭代

|Function函数|Description描述|
|-|-|
|`enumerate()`|
|`range()`|
|`zip()`|

---

### 函数式高阶函数

|Function函数|Description描述|
|-|-|
|`map()`|
|`reduce()`|
|`filter()`|

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Chapter :ok:
