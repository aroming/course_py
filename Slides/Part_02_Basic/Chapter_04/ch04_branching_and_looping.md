---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 流程控制（分支与循环）Control Flow (Branching and Looping)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 条件表达式Conditional Expression

---

### true与false

1. 条件为`false`的情况：
    1. `False`
    1. 数值零： `0`, `0.0`, `0j`
    1. 对象空
1. 条件为`true`的情况：
    1. `True`
    1. 数值非零
    1. 对象非空

---

```python {.line-numbers}
if 666:            #使用整数作为条件表达式
    print(9)

i = s = 0
while i <= 10:      #使用关系表达式作为条件表达式
    s += i
    i += 1

a = [3, 2, 1]
if a:              #使用列表作为条件表达式
    print(a)

a = []
if a:               #空列表等价于False
    print(a)
else:
    print('empty')
```

---

### 关系运算符与逻辑运算符

1. `Python`中的关系运算符可以连续使用（优缺点分别是什么 :question:）
1. `Python`的条件表达式中不允许使用赋值运算符（避免将`=`误当作`==`）
1. `Python`的双目逻辑运算符也具有 _短路原则_

---

```python {.line-numbers}
if age = 40: # invalid syntax
    print(age)

if False and something(): # something() will never execute
    another() # another() will never execute

if True or something(): # something() will never execute
    another() # another() will execute
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 分支Branching

---

```python {.line-numbers}
# single branching
if <condition expression>:
    <statement block>       # at least one statement

# double branchings
if <condition expression>:
    <statement block>
else:
    <statement block>

# multi branchings
if <condition expression>:
    <statement block>
elif <condition expression>:
    <statement block>
...
else:
    <statement block>
```

---

```python {.line-numbers}
# nested branchings
if <condition expression>:
    <statement block>
    if <condition expression>:
        <statement block>
    else:
        <statement block>
else:
    <statement block>
    if <condition expression>:
        <statement block>
```

---

```python {.line-numbers}
numbers_string = "123 456"
a, b = map(int, numbers_string.split())
if a > b:
    a, b = b, a
print(a, b)
```

---

```python {.line-numbers}
chicken_rabbit_number, leg_number = \
    map(int, input("请输入鸡兔总数和腿总数：").split())
rabbit_number = (leg_number - chicken_rabbit_number * 2) / 2
if int(rabbit_number) == rabbit_number:
    rabbit_number = int(rabbit_number)
    chicken_number = int(chicken_rabbit_number - rabbit_number)
    print("鸡：{0},兔：{1}".format(chicken_number, rabbit_number))
else:
    print("数据不正确，无解")
```

---

```python {.line-numbers}
score = int(input("请输入一个整数："))
if score > 100 or score < 0:
    print("wrong score...it must between 0 and 100...")
elif score >= 90:
    print('A')
elif score >= 80:
    print('B')
elif score >= 70:
    print('C')
elif score >= 60:
    print('D')
else:
    print('F')
```

---

```python {.line-numbers}
score = int(input("请输入一个整数："))
if score > 100 or score < 0:
    print("wrong score...it must between 0 and 100...")
    exit(1)
degrees = "DCBAAF"
index = (score - 60) // 10
if index >= 0:
    print(degrees[index])
else:
    print(degrees[-1])
```

---

### 三目运算符

```python {.line-numbers}
# in Python
<value_if_true> if <condition> else <value_if_false>
```

```c {.line-numbers}
//in C/C++/Java
<condition> ? <value_if_true> : <value_if_false>
```

---

### What Would Below Codes Output :question:

```python {.line-numbers}
a = 11
b = 6 if a > 13 else 9
print(type(b))
b = (6 if a > 13 else 9)
print(type(b))
b = (6 if a > 13 else 9,)
print(type(b))
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 循环Loops

---

### for-in Loop and while Loop

1. `while`一般用于循环次数难以提前确定的情况（条件循环）
1. `for-in`一般用于循环次数可提前确定或遍历访问的情况（次数循环或迭代循环）

---

```python {.line-numbers}
# while loop
while <condition>:
    <statement block>
[else:      # 条件不成立时执行else，break时不执行else
    <statement block>]

# for loop
for <iterating_var> in <sequence>:
    <statement block>
[else:      # 迭代结束时执行else，break时不执行else
    <statement block>]
```

---

```python {.line-numbers}
print("using while loop...")
i = 1

while i < 101:
    if i % 7 == 0 and i % 5 != 0:
        print(i, end='\t')
    i += 1

print("\n\nusing for-in loop...")

for i in range(1, 101):
    if i % 7 == 0 and i % 5 != 0:
        print(i, end='\t')
```

---

### break和continue

1. `break`： 结束本层循环
1. `continue`： 结束本次循环，进入下一次循环

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Chapter :ok:
