---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 面向对象程序设计Object-Oriented Programming

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 面向对象Object-Oriented

---

### 什么是面向对象

1. `object对象`： $\text{数据} + \text{行为（对数据内容的操作）}$
1. `class类`： $\text{数据结构} + \text{算法（业务逻辑）}$
1. `class`是`object`的模板（结构），`object`是`class`的实例
1. `class`是静态的、存储于`.text`，`object`是动态的、存储于`.data`

---

### 什么是面向对象（contd.）

1. `OO（Object-Oriented，面向对象）`： 将对象作为一个完整的实体进行抽象、设计和编码
    1. `OOA（Object-Oriented Analysis，面向对象分析）`： 抽象
    1. `OOD（Object-Oriented Design，面向对象设计）`： 设计
    1. `OOP（Object-Oriented Programming，面向对象编程）`： 编码
1. 面向对象的三大特征： `encapsulation封装`、`inheritance继承`、`polymorphism多态`

---

```python {.line-numbers}
class <class_name>([base_class]):
    [class_member_property]

    [
    def <member_method>:
        <statement block>
    ]
```

---

### 面向对象的基本术语

1. `class类`
1. `object对象`： 在`OO`中，特指`class类`的`instance实例`
1. `attribute属性/property财产、属性/field字段`： `class`内定义的、属于`class`或`object`的变量（`method`中的局部变量不属于`class`或`object`）
1. `method方法`： `class`内定义的函数
1. `constructor构造方法`： 创建`instance`时用于初始`object`的`method`
1. `destructor析构方法`： 释放`instance`时用于清理`object`的`method`

---

### self与cls

1. `self`： 指向`object`本身的变量
1. `cls`： 指向`class`本身的变量
1. 调用`instance method`或`class method`时，`self`或`cls`实参由解释器自动推断并自动传入

:warning: `self`和`cls`只是名字上的约定俗成，可以使用其他形参名字（但不建议），`self`和`cls`也不是`keyword`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 特殊成员与访问控制

---

### 访问权限

1. 公有成员： 公共访问权限
1. 保护成员： 类内或子类访问权限
    1. 单下划线开头
    1. :exclamation: 保护成员仅为约定，实际上，类外可直接访问保护成员
1. 私有成员： 类内访问权限
    1. 双下划线开头、不以双下划线结尾
    1. :exclamation: `Python`使用`name mapping`将私有成员名字前加`_<class_name>`，此时，类外无法通过原名字访问

:point_right: 避免使用保护成员

---

```python {.line-numbers}
class Demo_Permission():

    def __init__(self):
        self._protected_member = 100
        self.__private_member = 200


demo = Demo_Permission()

print("access protected member outside class will ok ==> " + str(demo._protected_member))
print("access private member outside class will ok ==> " + str(demo._Demo_Permission__private_member))

"""will throw AttributeError"""
# print("access private member outside class ==> "+str(demo.__private_member))
```

---

### 特殊成员

1. 特殊成员： `Python interpreter`定义的具有特殊用途的成员，**公共访问权限**

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 属性和方法

---

### Attribute属性

1. 实例属性： 属于`object`的数据，每个`instance`拥有一份数据
    1. 定义方式： 在成员方法中定义（不在成员方法外定义），一般通过`__init__()`定义
    1. 访问方式： 通过`object`访问
1. 类属性： 属于`class`的数据，`class`的所有`instance`共享一份数据
    1. 定义方式： 在类中定义
    1. 访问方式： 通过`class`或`object`访问

---

```python {.line-numbers}
class SingleInstance:
    num = 0  # class property

    def __init__(self, name):
        if SingleInstance.num > 0:  # access through classname
            raise Exception("只能创建一个对象")
        SingleInstance.num += 1
        self.__name = name  # access through object


t1 = SingleInstance("69lisi")
```

---

### Method方法

1. `instance method实例方法`： 属于`object`的方法，通过`object`访问
    1. 首个形参为`self`
1. `class method类方法`： 属于`class`的方法，通过`class`或`object`访问
    1. 首个形参为`cls`
    1. `@classmethod`修饰
1. `static method静态方法`： 与`class`相关的方法，可直接访问（也可通过`class`或`object`访问）
    1. `@staticmethod`修饰

:warning: `class method`和`static method`不能 **_直接_** 访问实例属性、实例方法

---

```python {.line-numbers}
class Root:
    __total = 0  # class property

    def __init__(self, v):  # constructor
        self.__value = v  # instance property, access by object
        Root.__total += 1  # class property, access by class

    def show(self):  # instance method
        print("self.__value:", self.__value)
        print("Root.__total:", Root.__total)

    @classmethod
    def class_show_total(cls):  # class method
        print(cls.__total)

    @staticmethod
    def static_show_total():  # static method
        print(Root.__total)


r = Root(3)
r.show()
r.class_show_total()
r = Root(3)
r.static_show_total()
```

---

### 类的名字空间

1. `global namespace`
1. `class namespace`
1. `object namespace`
1. `local namespace`

---

### 类的名字空间访问规则

1. 类外： 后定义的方法覆盖先定义的方法
1. 类内、方法外： `class namespace`
1. 方法内
    1. `self`指定为`object namespace`
    1. `cls`指定为`class namespace`
    1. 无`self`或`cls`时，为`local namespace`

---

### 属性方法

1. 通过`@property`定义 **_类外_** 只读属性
1. 通过`property()`定义读、写、删除权限属性

```python {.line-numbers}
class property(fget=None, fset=None, fdel=None, doc=None)
```

><https://docs.python.org/3.9/library/functions.html#property>

---

```python {.line-numbers}
class C:
    def __init__(self):
        self._x = None

    @property
    def x(self):
        """I'm the 'x' property."""
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @x.deleter
    def x(self):
        del self._x
```

---

```python {.line-numbers}
class C:
    def __init__(self):
        self._x = None

    def getx(self):
        return self._x

    def setx(self, value):
        self._x = value

    def delx(self):
        del self._x

    x = property(getx, setx, delx, "I'm the 'x' property.")
```

---

```python {.line-numbers}
class Test:
    def __init__(self, value):
        self.__rwd_value = value
        self.__ro_value = value

    @property
    def ro_value(self): # outer-class use by object.ro_value
        return self.__ro_value

    def __get_rwd_value(self):
        return self.__value

    def __set_rwd_value(self, v):
        self.__value = v

    def __del_rwd_value(self):
        del self.__value

    value = property(__get_rwd_value, __set_rwd_value, __del_rwd_value)

    def show(self):
        print(self.__value)

    def modify_ro_value(self):
        self.__ro_value = 5  # can modify inside class


test = Test(3)
print(test.ro_value)
test.modify_ro_value()
print(test.ro_value)
test.rwd_value = 12  # modify outside class
print(test.rwd_value)
# test.ro_value = 10  # cannot modify outside class
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 继承Inheritance

---

### `Python`的继承

1. 继承是一种复用机制，在一个现有的类的基础上进行扩展
1. 被继承的类称为“父类/基类”，继承的类称为“子类/派生类”
1. 子类继承了基类的公有成员和保护成员，不继承基类的私有成员
1. 子类通过`super([<base_clsname>[, self]])`或`<base_clsname>.`访问基类成员
1. 多重继承时，如果多个基类中有相同的成员，而子类在使用时未指定明确的基类，则按继承声明从左到右顺序继随第一个匹配的成员

---

```python {.line-numbers}
class C(B):
    def method(self, arg):
        super().method(arg)    # This does the same thing as:
                               # super(C, self).method(arg)
```

---

```python {.line-numbers}
class Person(object):
    def __init__(self, name='', age=20, sex='man'):
        self.set_name(name)
        self.set_age(age)
        self.set_gender(sex)

    def set_name(self, name):
        if not isinstance(name, str):
            raise Exception('name must be string.')
        self.__name = name

    def set_age(self, age):
        if type(age) != int:
            raise Exception('age must be integer.')
        self.__age = age

    def set_gender(self, gender):
        if gender not in ('male', 'female'):
            raise Exception('gender must be "male" or "female"')
        self.__gender = gender

    def show(self):
        print(self.__name, self.__age, self.__gender, sep='\n')
```

---

```python {.line-numbers}
class Teacher(Person):
    def __init__(self, name='', age=30,
                 gender='male', department='Computer'):
        super(Teacher, self).__init__(name, age, gender)  # Person.__init__(self, name, age, sex)
        self.set_department(department)

    def set_department(self, department):
        if type(department) != str:
            raise Exception('department must be a string.')
        self.__department = department

    # override
    def show(self):
        super(Teacher, self).show()
        print(self.__department)
```

---

```python {.line-numbers}
if __name__ == '__main__':
    zhangsan = Person('Zhang San', 19, 'male')
    zhangsan.show()
    print('=' * 30)

    lisi = Teacher('Li si', 32, 'male', 'Math')
    lisi.show()
    lisi.set_age(40)
    lisi.show()
```

---

### `Python`继承的缺陷

```python {.line-numbers}
class A:
    def __init__(self):
        self.__name = 'python'  # 翻译成self._A__name='python'


class A(A):  # 派生类和基类取相同的名字就可以使用基类的私有变量。
    def func(self):
        print(self.__name)  # 翻译成print self._A__name


instance = A()
instance.func()
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Special Methods/Magic Methods特殊方法/魔术方法

>1. <https://docs.python.org/3/reference/datamodel.html#special-method-names>
>1. <https://pycoders-weekly-chinese.readthedocs.io/en/latest/issue6/a-guide-to-pythons-magic-methods.html>

---

```python {.line-numbers}
object.__new__(cls[, ...]) # Called to create a new instance of class cls.
object.__init__(self[, ...]) # Called after the instance has been created (by __new__()), \
                            # but before it is returned to the caller.
object.__del__(self) # Called when the instance is about to be destroyed.

"""class by built-in function"""
object.__repr__(self)
object.__str__(self)
object.__bytes__(self)
object.__format__(self, format_spec)
object.__hash__(self)
object.__bool__(self)
```

---

```python {.line-numbers}
# operator overloading运算符重载
object.__add__(self, other)
object.__sub__(self, other)
object.__mul__(self, other)
object.__matmul__(self, other)
object.__truediv__(self, other)
object.__floordiv__(self, other)
object.__mod__(self, other)
object.__divmod__(self, other)
object.__pow__(self, other[, modulo])
object.__lshift__(self, other)
object.__rshift__(self, other)
object.__and__(self, other)
object.__xor__(self, other)
object.__or__(self, other)
```

---

```python {.line-numbers}
object.__getitem__(self, key)
object.__setitem__(self, key, value)
object.__delitem__(self, key)
object.__missing__(self, key)
```

---

```python {.line-numbers}
class Demo:
    def __init__(self, value: int) -> None:
        self.__value = value

    def __int__(self) -> None:
        return self.__value

    def __str__(self) -> str:
        return f"value is {self.__value}"

    def __add__(self, other):
        return Demo(self.__value + int(other))


demo = Demo(3)
other_demo = Demo(4)
print((demo + 3))
print(demo + other_demo)
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
