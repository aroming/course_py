# PY-E03：Python函数设计

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：4
1. 实验目的：
    1. 掌握`Python`的函数的定义和使用
    1. 掌握`Python`解决具体问题的基本思路
1. 实验内容与要求：
    1. 实验内容01： 字母大小写个数判断
    1. 实验内容02： 选择法排序
    1. 实验内容03： 二分法查找
1. 实验条件：
    1. 硬件环境： `PC`
    1. 软件环境： `IDLE`或`Jupyter`或`PyCharm`

## 实验注意事项

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验前提条件与储备知识

## 实验指引

### 实验内容01

#### :microscope: 实验内容01具体任务

1. 程序目标： 判断一个字符串中大小写字母的个数
1. 程序要求：
    1. 在程序运行开始输出两行文字，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning:请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E03 content 01...`
    1. 定义一个函数，接收字符串形参，返回`tuple`，该`tuple`第一个元素为大写字母的个数，第二个元素为小写字母的个数

#### :ticket: 实验内容01参考截图

![](./assets_image/E0301_code.png)

#### :bookmark: 实验内容01相关知识

### 实验内容02

#### :microscope: 实验内容02具体任务

1. 程序目标： 选择法排序
1. 程序要求：
    1. 在程序运行开始输出两行文字，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning:请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E03 content 02...`
    1. 使用`math.randint`随机生成20个元素的`list`
    1. 分别进行正序排序和逆序排序并输出

#### :ticket: 实验内容02参考截图

![](./assets_image/E0302_code.png)

#### :bookmark: 实验内容02相关知识

### 实验内容03

#### :microscope: 实验内容03具体任务

1. 程序目标： 二分查找
1. 程序要求：
    1. 在程序运行开始输出两行文字，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning:请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E03 content 03...`
    1. 二分查找函数查找成功时返回第一次查找成功所在下标，查找不成功时返回`None`
    1. 使用`math.randint`随机生成20个元素的`list`，输出查找结果

#### :ticket: 实验内容03参考截图

![](./assets_image/E0303_code.png)

#### :bookmark: 实验内容03相关知识

## 延伸阅读
