# PY-E01： Python开发环境与“HelloWorld”程序设计

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前置条件](#实验前置条件)
3. [实验储备知识](#实验储备知识)
4. [实验注意事项](#实验注意事项)
5. [实验指引](#实验指引)
    1. [实验内容01： 安装及配置`Python`开发环境](#实验内容01-安装及配置python开发环境)
        1. [:microscope: 实验内容01具体步骤](#microscope-实验内容01具体步骤)
        2. [:ticket: 实验内容01参考截图](#ticket-实验内容01参考截图)
        3. [:bookmark: 实验内容01相关知识](#bookmark-实验内容01相关知识)
    2. [实验内容02： 安装`Python`扩展模块](#实验内容02-安装python扩展模块)
        1. [:microscope: 实验内容02具体步骤](#microscope-实验内容02具体步骤)
        2. [:ticket: 实验内容02参考截图](#ticket-实验内容02参考截图)
        3. [:bookmark: 实验内容02相关知识](#bookmark-实验内容02相关知识)
    3. [实验内容03： 设计“`HelloWorld`”程序](#实验内容03-设计helloworld程序)
        1. [:microscope: 实验内容03具体步骤](#microscope-实验内容03具体步骤)
        2. [:ticket: 实验内容03参考截图](#ticket-实验内容03参考截图)
        3. [:bookmark: 实验内容03相关知识](#bookmark-实验内容03相关知识)
6. [延伸阅读](#延伸阅读)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：综合性
1. 实验学时：4
1. 实验目的：
    1. 掌握`Python`基本开发环境搭建与配置
    1. 掌握`Python`基本开发流程
1. 实验内容与要求：
    1. 实验内容01： 安装及配置`Python`开发环境，分别安装和配置官方`Python`和`Anaconda`
    1. 实验内容02： 安装`Python`扩展模块，分别使用`pip`和`conda`安装模块
    1. 实验内容03： 设计`HelloWorld`程序，分别使用`IDLE`、`script`和`Jupyter`开发`HelloWorld`程序
1. 实验条件：
    1. 硬件环境： `PC`
    1. 软件环境： 官方`Python`、`Anaconda`

## 实验前提条件与储备知识

1. 了解`CLI`命令的基本结构
1. 了解`CLI`和`script`

## 实验注意事项

1. 使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. “打开`所有程序 ==> XXX`”表示点击`所有程序`后再点击`XXX`从而打开`XXX`程序
1. 请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体步骤** ==> 实验的具体任务及其步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验指引

### 实验内容01： 安装及配置`Python`开发环境

#### :microscope: 实验内容01具体步骤

1. 安装官方`Python`
    1. 从<https://www.python.org/downloads/>下载对应的安装包并安装（按默认安装）
    1. 打开`CMD命令提示符`，输入并运行`python --version`观察输出结果
    1. 打开`CMD命令提示符`，输入并运行`python -V`观察输出结果
1. 从<https://www.anaconda.com/products/individual>下载对应的安装包并安装（按默认安装）
    1. 打开`所有程序 ==> Anaconda3 ==> Anaconda Prompt(Anaconda3)`
    1. 在`Anaconda Prompt(Anaconda)`中，输入并运行`python --version`观察输出结果
1. 配置`Anaconda`的`Jupyter`为无`token`登录模式
    1. 打开`所有程序 ==> Anaconda3 ==> Anaconda Prompt(Anaconda3)`
    1. 在`Anaconda Prompt(Anaconda)`中，输入并运行`jupyter notebook --generate-config`
    1. 打开上一步骤输出结果提示的配置文件路径
    1. 使用`NotePad++`或其他文本文件编辑器打开配置文件`jupyter_notebook_config.py`
    1. 查找并定位到`#c.NotebookApp.token = '<generated>'`，将其修改为`c.NotebookApp.token = ''`
    1. 打开`所有程序 ==> Anaconda3 ==> Jupyter Notebook(Anaconda3)`（:exclamation: **打开后请勿关闭该窗口**）
    1. 打开`Firefox`或`Chrome`（:exclamation: **请勿使用`IE`**）打开网址`localhost:8888`

#### :ticket: 实验内容01参考截图

暂无

#### :bookmark: 实验内容01相关知识

暂无

### 实验内容02： 安装`Python`扩展模块

#### :microscope: 实验内容02具体步骤

1. 通过`pip`安装`pycodestyle`
    1. 打开`CMD命令提示符`
    1. 输入并运行`pip install pycodestyle`
    1. 完成`pycodestyle`安装
1. 通过`conda`安装`pycodestyle`
    1. 打开`所有程序 ==> Anaconda3 ==> Anaconda Prompt(Anaconda3)`
    1. 输入并运行`conda install pycodestyle`
    1. 按提示输入`y`
    1. 完成`pycodestyle`安装

#### :ticket: 实验内容02参考截图

暂无

#### :bookmark: 实验内容02相关知识

暂无

### 实验内容03： 设计“`HelloWorld`”程序

#### :microscope: 实验内容03具体步骤

1. `HelloWorld`程序要求（:exclamation: 请将`69lisi`修改为你自已的学号和姓名的全拼）
    1. 将`69lisi`赋值给`my_name`变量
    1. 通过`my_name`变量输出`Hello World...My Name is => 69lisi`
1. 通过`IDLE`设计并运行`HelloWorld`程序
1. 通过新建`hello_world.py`并运行`python hello_world.py`设计并运行`HelloWorld`程序
1. 通过`Jupyter`设计并运行`Helloworld`程序

#### :ticket: 实验内容03参考截图

暂无

#### :bookmark: 实验内容03相关知识

暂无

## 延伸阅读

1. [廖雪峰 -- Python教程 -- 第一个Python程序](https://www.liaoxuefeng.com/wiki/1016959663602400/1016966022717728)
