# Show Off Codes

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

## if statement

```python {.line-numbers}
# 01 - 01 ==> if...else...
if age > 18:
    stat = "adult"
else:
    stat = "minor"

# 01 - 02 ==> <true_expression> if <condition> else <false_expression>
stat = "adult" if age > 18 else "minor"

# 02 - 01 ==> <condition> and <true_expression> or <false_expression>
stat = age > 18 and "adult" or "minor"

# 02 - 02 ==> (<condition> and (<true_expression>,) or (<false_expression>,))[0]
stat = ((age > 18) and ("adult",) or ("minor", ))[0]

# 03 -01 ==> (<false_expression>, <true_expression>)[<condition>]
stat = ("minor", "adult")[age > 18]

# 03 - 02 ==> (lambda: <false_expression>, lambda: <true_expression>)[<condition>]()
stat = (lambda: "minor", lambda: "adult")[age > 18]()

# 03 - 03 ==> {True: <true_expression>, False: <false_expression>}[<condition>]
stat = {True: "adult", False:  "minor"}[age > 18]
```
