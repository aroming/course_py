# Built-In Function

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

## Overview

1. <https://docs.python.org/3/library/functions.html>

截止`Python 3.9.0`一共有69个`built-in function`，可大致将其分成14大类，具体如下：

1. 数值
    1. 数值类型生成： `bool()`, `int()`, `float()`, `complex()`
    1. 进制转换： , `bin()`, `hex()`, `oct()`
    1. 数值计算/数理统计： `abs()`, `min()`, `max()`, `sum()`, `pow()`, `divmod()`, `round()`
1. 数据结构
    1. 序列结构类型生成： `list()`, `tuple()`, `zip()`, `enumerate()`
    1. 集合结构类型生成： `set()`, `dict()`, `frozenset()`
    1. 字符串类型生成： `str()`
    1. 字节数组生成： `bytes()`, `bytearray()`, `memoryview()`
    1. 字符串操作函数： `format()`, `chr()`, `ord()`, `ascii()`
    1. 数据结构相关函数： `len()`, `sorted()`, `reversed()`, `slice()`
1. 逻辑判断： `all()`, `any()`, `callable()`, `isinstance()`, `issubclass()`
1. 作用域： `locals()`, `globals()`
1. 迭代器生成器： `range()`, `next()`, `iter()`
1. 输入输出： `print()`, `input()`
1. 文件： `open()`
1. 模块： `__import__()`
1. 帮助： `help()`
1. 对象相关： `dir()`, `hash()`, `repr()`, `type()`, `id()`
1. 面向对象： `setattr()`, `delattr()`, `getattr()`, `hasattr()`, `super()`, `property()`, `vars()`, `classmethod()`, `staticmethod()`
1. 动态运行： `eval()`, `exec()`, `compile()`
1. 函数式： `map()`, `reduce()`, `filter()`
1. 调试： `breakpoint()`

:warning: 此分类为非官方分类，也并非完全准确，仅为方便理解和学习，部分函数也可能同时与多个类别相关，如`enumerate()`也与“迭代器生成器”相关。
